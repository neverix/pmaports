# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=polkit-kde-agent
pkgver=5.15.5
pkgrel=0
pkgdesc="Daemon providing a polkit authentication UI for KDE"
arch="all"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0"
depends_dev="qt5-qtbase-dev kiconthemes-dev kdbusaddons-dev kcrash-dev polkit-qt-dev ki18n-dev kwindowsystem-dev kwidgetsaddons-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-1-$pkgver.tar.xz"
subpackages="$pkgname-lang"
builddir="$srcdir/$pkgname-1-$pkgver"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_LIBEXECDIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="030bbff94517a02f40bfdbaf0142834d95fc5299fcb126f3014268233e24598aecbad8b04ac1b6c4d3a0d4821a73b10ecba84f2e80af53782bf137e151455ed9  polkit-kde-agent-1-5.15.5.tar.xz"
