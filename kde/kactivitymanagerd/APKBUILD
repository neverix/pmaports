# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivitymanagerd
pkgver=5.15.5
pkgrel=0
pkgdesc="System service to manage user's activities and track the usage patterns"
arch="all"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0"
depends_dev="qt5-qtbase-dev kio-dev kdbusaddons-dev ki18n-dev kconfig-dev kcoreaddons-dev kwindowsystem-dev kglobalaccel-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kwidgetsaddons-dev kservice-dev kbookmarks-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev"
makedepends="$depends_dev extra-cmake-modules boost-dev"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-lang"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="9c9c066ccf7b56ecb0816be5db82dec7d89f9a834ef75a0f96d073d0d15470a57a84f811c17feb4edbf262fc44275d943fd7e06ec2bdb53860a4f6a4ea1b64d3  kactivitymanagerd-5.15.5.tar.xz"
